[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687040.svg)](https://doi.org/10.5281/zenodo.4687040)

# Heating Degree Days (HDD) for the reference period 2002-2012


## Repository structure

```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the Heating Degree Days (HDD, 15/18) for the reference period 2002-2012 in EU28 on hectare (ha) level.

For the HDD, we use the observed average daily temperatures on the 25 x 25 km raster [1] and apply an environmental lapse rate of 6.5°C per 1000 m elevation gain according to the specifications of the International Standard Atmosphere model using the digital elevation model over Europe (EU‐DEM) layer on the 30 x 30 m grid level [2].

The useful energy demand (UED) for space heating (SH) on a local level are corrected by applying the ratio between the calculated site specific heating degree days and the heating degree days on NUTS3 level using an elasticity of 0.75, the energy needs for domestic hot water preparation is considered to be independant from the HDD.By reducing the weight of the local heating degree‐days we account for uncertainties involved as well as the (plausible) assumption, that in general buildings in colder areas (on higher elevation) might already have a higher energy performance than those in warmer (lower) areas.
The share of energy needs for space heating on the sum of space heating and domestic hot water preparation is estimated based on the following equation:
  
  EnergyHeating3000 = 6
  EnergyDHW = 1
  
  EnergyHeatingHDD = EnergyHeating3000 * ((Average_HDD_NUTS0_POPULATION_weighted) / 3000) ** 0.6
  ShareHeating = EnergyHeatingHDD / (EnergyHeatingHDD + EnergyDHW)


The HDD dataset is used as an indicator for Heating demand in the Hotmaps toolbox.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 33 and page 39.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
The obtained CDD Raster, although is plausible but has uncertainties as well due to a high resolution.


### References
[1] Haylock, M.R., van den Besselaar, E. J. M., van der Schrier, G., and Klein Tank, A. M. G., “A European daily high resolution observational gridded data set of sea level.

[2] [European Environmental Agency, “Copernicus Land Monitoring Service  EU‐DEM,” 2017.](https://www.eea.europa.eu/data-and-maps/data/copernicus-land-monitoring-service-eu-dem)


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Ku¨hnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf)


## Authors
Andreas Mueller <sup>*</sup>, Mostafa Fallahnejad <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien
[e-think energy research](https://www.e-think.ac.at/), Argentinierstrasse 18/10, 1040 Wien


## License

Copyright © 2016-2019: Andreas Mueller, Mostafa Fallahnejad
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

